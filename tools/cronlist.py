#!/usr/bin/env python
# vi: et sw=4 fileencoding=utf-8
#
# Copyright 2019, Harry Karvonen <harry.karvonen@onebyte.fi>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#

import argparse

from datetime import datetime, timedelta

from crontab import CronTab

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--user',
        '-u',
        default=True,
        metavar='username',
    )
    parser.add_argument(
        '--date',
        '-d',
        type=lambda s: datetime.strptime(s, '%Y-%m-%dT%H:%M:%S'),
        metavar='%Y-%m-%dT%H:%M:%S',
    )
    parser.add_argument(
        '--hours',
        type=int,
        default=1,
    )
    parser.add_argument(
        '--end',
        '-e',
        type=lambda s: datetime.strptime(s, '%Y-%m-%dT%H:%M:%S'),
        metavar='%Y-%m-%dT%H:%M:%S',
    )

    args = parser.parse_args()

    now = args.date or datetime.now()
    end = args.end or (now + timedelta(hours=args.hours))

    ct = CronTab(user=args.user)
    lls = []
    for l in ct:
        if not l.is_enabled():
            continue
        n = now
        while n < end:
            n = l.schedule(n).get_next()
            if n <= end:
                lls.append((n, l))
    lls.sort(key=lambda k: k[0])

    for ll in lls:
        print(ll[0].strftime('%H:%M'), ll[1].command)

if __name__ == '__main__':
    main()
